struct page johan_pages[] =
{
	{	// startrummet
		"Du vaknar upp i ett mörkt rum. Du minns inte hur du hamnade här. När du ser dig omkring verkar du befinna dig i någon slags lobby.  I taket hänger en stor neonskylt som lyder IF Skadeförsäkring. Du vill ta dig härifrån. Du ser 3 stycken dörrar på vardera sida och alla är på glänt. Vilken dörr väljer du?",
		3,
		0,
		{
			{1,0,"Gå till vänster"},
			{2,0,"Gå rakt fram"},
			{3,0,"Gå till höger"},
		}
	},
	{	// rum 1
		"I det här rummet snurrar en stor fläktlampa. Swooosh swooosh. På ena väggen hänger en stor tavla på en nyckel med texten 'Hittar du mig så hittar du ut!' Du ser två dörrar på vardera sida. Vad väljer du?.",
		3,
		0,
		{
			{4,0,"Gå till vänster"},
			{5,0,"Gå till höger"},
			{0,0,"Gå bakåt"},
		
		}
	},
	{	// rum 2
		"Du känner en stank från ena hörnet. Där ligger en Switch och sover. 'Den måste vara på ett break' tänker du då den har lagt ett 'case' som huvudkudde. Du ser en liten ventilationsöppning till vänster.",
		2,
		0,
		{
			{5,0,"Kryp genom ventilationen"},
			{0,0,"Gå bakåt"},
		}
	},
	{	// rum 3
		"Rummet är varmt och fuktigt och verkar vara något slags tropicarium. Det är fullt med fjärilar överallt så du måste hålla andan. Du ser ett valv till vänster som du kan gå igenom!",
		2,
		0,
		{
			{6,0,"Gå igenom valvet"},
			{0,0,"Gå tillbaks till första rummet"},
		}
	},
	{	// rum 4
		"Du kommer in i ett ovalt rum med en stor tavla på Donald Trump. Vid sidorna av tavlan brinner två facklor och på golvet ligger en liten miniatyr av en oljerigg. Till vänster ser du ett stort kassavalv med en stor mekanism som du tror man kan snurra på. Du hittar även en kofot!",
		2,
		1,
		{
			{8,0,"Öppna dörren"},
			{1,0,"Gå tillbaks"},
		}
	},
	{	// rum 5
		"Du verkar ha kommit in i ett maskinrum. Mitt i rummet finns en stor generator som brummar och glöder. Det är så pass dammigt att du börjar hosta okontrollerat. I ena hörnet ser du en liten ventilationsöppning. Du vill snabbt ta dig härifrån!",
		2,
		0,
		{
			{2,0,"Kryp genom ventilationen"},
			{1,0,"Gå genom dörren"},
		}
	},
	{	// rum 6
		"Värmen fortsätter in i det här rummet och du ser ett djupt hål full med krokodiler. Det finns en list att gå på runt hålet och en tung kedja att hålla sig i. Rakt fram finns det en skjutdörr av glas. Den verkar ha fastnat i ett läge mellan öppen och stängd.",
		2,
		0,
		{
			{7,1,"Bänd upp dörren"},
			{3,0,"Gå tilbaks"},
		}
	},
	{	// rum 7
		"Du kommer in i ett litet rum med metallväggar fullt med kondens. I taket hänger det ett rep.",
		1,
		0,
		{
			{-1,0,"Klättra upp för repet"},
		}
	},
	{	// rum återvändsgränd
		"Du försöker öppna dörren, men den är låst!",
		1,
		0,
		{
			{4,0,"Återgå till rummet"},
		}
	},

};

