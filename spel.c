#include <stdio.h>
#include <stdlib.h>

enum {
	NO_KEY = 0,
	KEY_1 = 1<<1,
	KEY_2 = 1<<2,
	KEY_3 = 1<<3,
	KEY_4 = 1<<4,
	KEY_5 = 1<<5,
	KEY_6 = 1<<6,
	KEY_7 = 1<<7,
	KEY_8 = 1<<8,
	KEY_9 = 1<<9,
	KEY_10 = 1<<10,
	KEY_11 = 1<<11,
	KEY_12 = 1<<12,
	KEY_13 = 1<<13,
	KEY_14 = 1<<14,
	KEY_15 = 1<<15,
	
	KEY_IAN1 = 1<<5,
	KEY_IAN2 = 1<<6,
	KEY_IAN3 = 1<<7,
};

struct option {
	int page;	// Gå till sida när valet väljs
	int keymask;	// Kräv nyckel för att låsa upp alternativ
	char* text;	// text
};
struct page {
	char* text;	// text
	int count;	// Antal val
	int keymask;	// Ge nyckel när sidan visas.
	struct option options[5];
};

/* Keymask: (Binärt!)
 * 0 = kräv/ge ingen nyckel
 * 1 = Nyckel 1
 * 2 = Nyckel 2
 * 3 = Nyckel 1 & 2
 * etc...
 */

 
// kopiera följande struct och byt namn...

#define DEAD_MSG "\nSkriv 0 för att avsluta."

#include "ian.h"
#include "johansrum.h"
//#include "jonirum.h"
#include "robinslevel.h"

// kopiera följande struct och byt namn...
struct page placeholder[] =
{
	{
		"Sorry! Kompileringsfel!\n",
		1,
		0,
		{
			{100,0,"Gå tillbaka till första våningen"}
		}
	}
};

struct page hub_pages[] =
{
	{	// sida 0
		"Du står framför fyra dörrar...", // story
		4, // Antal val
		0, // nyckel
		{
			{101,0,"Gå genom dörr 1"}, // val 1
			{102,0,"Gå genom dörr 2"}, // val 2
			{103,0,"Gå genom dörr 3"}, // val 3
			{104,0,"Gå genom dörr 4"}, // val 4
		}
	},


};


struct page pages[] =
{
	{	// sida 0
		"Det här är sida 0", // story
		3, // Antal val
		0, // nyckel
		{
			{1,0,"gå till sida 1"}, // val 1
			{0,0,"gå till sida 0"}, // val 2
			{2,1,"Detta val kräver nyckeln."}, // val 3
		}
	},
	{	// sida 1
		"Det här är sida 1. Du har nu nyckel #1.",
		1, // antal val
		1, // nyckel
		{
			{0,0,"Gå till sida 0"} // val 1
		}
	},
	{
		// sida 2
		"Du hittade en trappa till nästa våning",
		1,
		0,
		{
			{-1,0,"Gå till nästa våning"}
		}
	},
	{
		"Du sitter i ett hörn på mölk",
		3,
		1,
		{
			{2,2,"Gå ut genom dörren"},
			{3,0,"Ta kaffe"},
			{4,0,"Sätt på datorn"}
		}
	}

};

// fyll i namnet på din struct här.
// Den första structen här körs när programmet börjar.
struct page* pagelist[] =
{
	hub_pages,
	ian_pages,
	johan_pages,
	placeholder, //joni_pages,
	robin_pages,
};

int main()
{
	setvbuf(stdout,0,_IONBF,0);

	int keymask=0;
	int page=0; // Första sidan
	int curr_page=page;
	int floor=0;

	int avail_opt[5];

	struct page *p;
	while(1)
	{
		
		// https://stackoverflow.com/a/33450696
		// clear screen för både windows & unix
		system("@cls||clear");
		
		p = &pagelist[floor][page];
		curr_page=page;

		// Lägg till nycklar om det är skriptat
		keymask |= p->keymask;
		// Visa sidtext
		printf("\n%s\n\n",p->text);

		// räkna ut alla tillgängliga val
		int avail = 0;
		for(int i=0;i<p->count;i++)
		{
			int km = p->options[i].keymask;
			// Finns alla nycklar som vi kräver för det här rummet...
			//printf("Debug alt %d km=%d, nu har vi %d\n",i,km,keymask);
			if((keymask & km) == km)
			{
				avail_opt[avail++] = i;
			}
		}

		// Visa alla val
		for(int i=0;i<avail;i++)
		{
			printf("Val %d: %s\n",i+1,p->options[avail_opt[i]].text);
		}
		int val;

		printf("\nGör ditt val: ");
		
		// scanf returnerar 0 ifall den inte kunde tyda vad man skrev.
		if(!scanf("%d",&val))
		{
			// ett problem med scanf är att om inte alla fält fylldes
			// in så kommer alla nästa anrop till scanf ge exakt
			// samma resultat, vilket kan potentiellt orsaka en endlös
			// loop. den här workaround funkar på windows och vissa
			// linux-system men är inte garanterad tyvärr.
			fflush(stdin);
			continue;	
		}
		
		// 0 stänger programmet
		if(val==0)
			return 0;
		// Annars gå till den länkade sidan
		else if(val<=avail)
		{
			page = p->options[avail_opt[val-1]].page;
		}

		// Special-rum
		if(page >= 200) // Jämför
		{
			printf("\nGe ditt svar: ");
			int svar,ratt = page-200;
			scanf("%d",&svar);
			if(svar == ratt)
				page = curr_page+1; // rum + 1 om rätt svar
			else
				page = curr_page+2; // rum + 2 om fel svar
			
		}
		else if(page >= 100) // Gå till våning
		{
			floor = page-100;
			page=0;
		}
		else if(page == -1) // Nästa våning
		{
			floor++;
			page=0;
		}
		else if(page == -2) // starta om från början
		{
			floor=0;
			page=0;
			keymask=0;
		}
	}

	return 0;
}


