struct page ian_pages[] =
{
	
/*
         (20)
	 3    19
	 2  1 17 18
	 7( 0)13
	 
	0 = start/skog
	7 = puzzel
	13= oändlig korridor
	20= till skogen
*/
	
	{	// rum 0
		"Plötsligt står du nu framför entrén till en gigantisk bas!\n"
		"Du ser en öppen dörr.",
		5,
		0,
		{
			{1,0,"Gå genom entrén"},
			{27,KEY_IAN1,"Gå norrut"},
			{27,KEY_IAN1,"Gå österut"},
			{27,KEY_IAN1,"Gå västerut"},
			{23,KEY_IAN1,"Gå söderut"}
		}
	},
	{	// rum 1
		"Du är nu i en ganska ljus korridor.\n"
		"Du ser en vit dörr, en svart dörr och entrén du kom in genom.",
		3,
		0,
		{
			{0,0,"Gå ut genom entrén"},
			{2,0,"Gå genom den vita dörren"},
			{17,0,"Gå genom den svarta dörren"},
		}
	},
	{	// rum 2
		"Du står på ena sidan av en en avlång korridor.\n"
		"Du ser en skylt, en röd dörr och en vit dörr.",
		4,
		0,
		{
			{3,0,"Gå till andra sidan"},
			{7,0,"Gå genom den röda dörren"},
			{1,0,"Gå genom den vita dörren"},
			{6,0,"Läs skylten"},
		}
	},
	{	// rum 3
		"Du står på andra sidan av en avlång korridor.\n"
		"Du ser ett anteckningsblock och en postit-lapp.",
		3,
		0,
		{
			{2,0,"Gå till ena sidan"},
			{4,0,"Kolla anteckningsblocket"},
			{5,0,"Kolla postit-lappen"},
		}
	},
	{	// rum 4
		"På anteckningsblocket står det text skriven på ett språk "
		"du inte förstår.\n"
		"Förutom texten finns det också en ritning på en kompass.",
		2,
		0,
		{
			{2,0,"Gå till ena sidan"},
			{5,0,"Kolla postit-lappen"},
		}
	},
	{	// rum 5
		"På postit-lappen står det följande:\n"
		"if(B1 && B3)\n"
		"\tunlock();\n"
		"else if(B3 || B1 && !B2)\n"
		"\talarm();",
		2,
		0,
		{
			{2,0,"Gå till ena sidan"},
			{4,0,"Kolla anteckningsblocket"},
		}
	},
	{	// rum 6
		"På skylten står det följande:\n"
		"FÖRRÅD SKYDDAT AV ALARMSYSTEM!\n"
		"INTRÅNG STRAFFAS MED DÖDEN!!!",
		3,
		0,
		{
			{3,0,"Gå till andra sidan"},
			{7,0,"Gå genom den röda dörren"},
			{1,0,"Gå genom den vita dörren"},
		}
	},
	{	// rum 7
		"Du ser ett kassaskåp. På låset finns det tre knappar.",
		4,
		0,
		{
			{8,0,"Tryck på knapp 1"},
			{9,0,"Tryck på knapp 2"},
			{8,0,"Tryck på knapp 3"},
			{2,0,"Gå tillbaka"},
		}
	},
	{	// rum 8
		"Efter att ha tryckt på en av knapparna hör du ljudet "
		"av en alarmsiren.\n"
		"Innan du har hunnit reagera dyker det upp en massa "
		"mördarrobotar!\n"
		"Du slits i stycken innan du ens har kunnat tänka på att fly.\n"
		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Börja om från början"}
		}
	},
	{	// rum 9
		"Du ser ett kassaskåp. På låset finns det tre knappar.\n"
		"Efter att du har tryckt på en knapp har dörren bakom dig "
		"låst sig!\n"
		"Knapp 2 lyser.",
		3,
		0,
		{
			{11,0,"Tryck på knapp 1"},
			{10,0,"Tryck på knapp 2"},
			{8,0,"Tryck på knapp 3"},
		}
	},
	{	// rum 10
		"Du ser ett kassaskåp. På låset finns det tre knappar.\n"
		"Efter att du har tryckt på en knapp har dörren bakom dig "
		"låst sig!",
		3,
		0,
		{
			{8,0,"Tryck på knapp 1"},
			{9,0,"Tryck på knapp 2"},
			{8,0,"Tryck på knapp 3"},
		}
	},
	{	// rum 11
		"Du ser ett kassaskåp. På låset finns det tre knappar.\n"
		"Efter att du har tryckt på en knapp har dörren bakom dig"
		"låst sig!\n"
		"Knapp 1 och 2 lyser.",
		3,
		0,
		{
			{9,0,"Tryck på knapp 1"},
			{8,0,"Tryck på knapp 2"},
			{12,0,"Tryck på knapp 3"},
		}
	},
	{	// rum 12
		"Efter den senaste knapptryckning öppnade kassaskåpet sig!\n"
		"Inuti kassaskåpet fanns en kompass som du tar.\n"
		"Dörren bakom dig är nu också upplåst.",
		1,
		KEY_IAN1,
		{
			{2,0,"Gå tillbaka"},
		}
	},
	{	// rum 13
		"Du befinner dig i en korridor som verkar oändlig.\n"
		"Det finns en dörr till sidan.\n"
		"Om du kollar bakom dig finns det en dörr.",
		3,
		0,
		{
			{14,0,"Gå framåt"},
			{17,0,"Gå genom dörren bakom dig"},
			{15,0,"Gå genom dörren till sidan"},
		}
	},
	{	// rum 14
		"Du befinner dig i en korridor som verkar oändlig.\n"
		"Det finns en dörr till sidan.\n"
		"Om du kollar bakom dig finns det en dörr.",
		3,
		0,
		{
			{13,0,"Gå framåt"},
			{17,0,"Gå genom dörren bakom dig"},
			{16,0,"Gå genom dörren till sidan"},
		}
	},
	{	// rum 15
		"Efter du passerade dörren låste den sig bakom dig.\n"
		"Efter ett tag hör du ljudet av en gas som frigörs.\n"
		"Inget tvek om saken - det är giftgas!\n"
		"Inom loppet av några minuter tappar du medvetandet och "
		"resten är historia.\n"
		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Börja om från början"}
		}
	},
	{	// rum 16
		"Du kliver in i ett nästan tomt rum.\n"
		"På golvet fanns en nyckel som du tar.",
		1,
		KEY_IAN2,
		{
			{14,0,"Gå tillbaka"},
		}
	},
	{	// rum 17
		"Du befinner dig i en korridor med vita väggar.\n"
		"Du ser en svart dörr, en blå dörr, grå dörr, en brun dörr "
		"och en kortläsare.",
		5,
		0,
		{
			{1,0,"Gå genom den svarta dörren"},
			{18,0,"Gå genom den grå dörren"},
			{19,0,"Gå genom den blå dörren"},
			{13,0,"Gå genom den bruna dörren"},
			{30,KEY_IAN3,"Använd kortläsaren"},
			
		}
	},
	{	// rum 18
		"I det här rummet finner du en karta över Linköping.\n"
		"Du märker att en ring har ritats omkring Mjärdevi med ett "
		"kryss i mitten.\n"
		"Någon har också skrivit: "
		"\"Nyckelordet är SNORGUBBSLAXFILE\"",
		1,
		0,
		{
			{17,0,"Gå tillbaka"}
		}
	},
	{	// rum 19
		"Du befinner dig i ett rum med svarta väggar.\n"
		"Du ser en blå dörr och en ytterdörr som leder till en skog.",
		2,
		0,
		{
			{17,0,"Gå tillbaka"},
			{20,0,"Gå mot skogen"}
		}
	},
	
	
/*
	        (1)
	   (25)  0
	26	22  23
	28	20  21
	   (19)
	   
	25 = skyddsrum
	19 = till basen.
	0  = till början
*/
	
	{	// rum 20
		"Du befinner dig djupt inne i skogen.\n"
		"Du ser spår av vargar på marken.\n"
		"Utan en kompass kan du nog inte ta dig längre!\n"
		"Du ser stigen som tar dig tillbaka till basen.",
		5,
		0,
		{
			{22,KEY_IAN1,"Gå norrut"},
			{21,KEY_IAN1,"Gå österut"},
			{28,KEY_IAN1,"Gå västerut"},
			{27,KEY_IAN1,"Gå söderut"},
			{19,0,"Gå tillbaka mot basen"}
		}
	},
	{	// rum 21
		"Du befinner dig djupt inne i skogen.\n"
		"Du ser en papperslapp under en sten.",
		5,
		0,
		{
			{23,KEY_IAN1,"Gå norrut"},
			{27,KEY_IAN1,"Gå österut"},
			{20,KEY_IAN1,"Gå västerut"},
			{27,KEY_IAN1,"Gå söderut"},
			{24,0,"Läs lappen"}
		}
	},
	{	// rum 22
		"Du befinner dig djupt inne i skogen.\n"
		"Du ser en dörr som verkar leda till ett skyddsrum, "
		"men den är låst.",
		5,
		0,
		{
			{27,KEY_IAN1,"Gå norrut"},
			{23,KEY_IAN1,"Gå österut"},
			{26,KEY_IAN1,"Gå västerut"},
			{20,KEY_IAN1,"Gå söderut"},
			{25,KEY_IAN2,"Gå genom dörren"}
		}
	},
	{	// rum 23
		"Du befinner dig djupt inne i skogen.\n",
		4,
		0,
		{
			{0, KEY_IAN1,"Gå norrut"},
			{27,KEY_IAN1,"Gå österut"},
			{22,KEY_IAN1,"Gå västerut"},
			{21,KEY_IAN1,"Gå söderut"}
		}
	},
	{	// rum 24
		"På lappen står det:\n"
		"\"Gå norrut annars dör du.\"",
		4,
		0,
		{
			{23,KEY_IAN1,"Gå norrut"},
			{27,KEY_IAN1,"Gå österut"},
			{20,KEY_IAN1,"Gå västerut"},
			{27,KEY_IAN1,"Gå söderut"}
		}
	},
	{	// rum 25
		"Du befinner dig i ett skyddsrum.\n"
		"Du ser dörren ut och en trappa. På en lapp står det "
		"\"Varning för monster\"",
		2,
		0,
		{
			{29,0,"Gå nerför trappan"},
			{22,0,"Gå ut"},
		}
	},
	{	// rum 26
		"Du befinner dig djupt inne i skogen.\n"
		"På marken fanns ett smutsigt passerkort som du tar.",
		4,
		KEY_IAN3,
		{
			{27,KEY_IAN1,"Gå norrut"},
			{22,KEY_IAN1,"Gå österut"},
			{27,KEY_IAN1,"Gå västerut"},
			{28,KEY_IAN1,"Gå söderut"}
		}
	},
	{	// rum 27
		"Utan att du hade märkt av det hade en varg förföljt dig\n"
		"under ditt äventyr i skogen!\n"
		"Till slut bestämde sig vargen för att ta sin chans och "
		"anfalla!\n"
		"Utan ett sätt att försvara sig blir du vargmat ganska fort.\n"
		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Börja om från början"}
		}
	},
	{	// rum 28
		"Du befinner dig djupt inne i skogen.\n"
		"Du hör ljudet av vargar.",
		4,
		0,
		{
			{26,KEY_IAN1,"Gå norrut"},
			{20,KEY_IAN1,"Gå österut"},
			{27,KEY_IAN1,"Gå västerut"},
			{27,KEY_IAN1,"Gå söderut"}
		}
	},
	{	// rum 29
		"I källaren fanns det inga monster men däremot en karta "
		"över skogen!\n"
		"Men kan du tyda den...?\n"
		"V V V s V\n"
		"V x r - V\n"
		"V - s - V\n"
		"V V V V V",
		1,
		0,
		{
			{25,0,"Gå upp igen"},
		}
	},
	{	// rum 30
		"Efter att ha använt passerkortet började ett larm tjuta.\n"
		"Ett par mördarrobotar dyker plötsligt upp, men istället "
		" för att\n"
		"mörda dig eskorterar de dig till ett hemligt rum.\n"
		"Robotarna säger \"LÖS PROBLEMET\" och ger dig ett papper.\n\n"
		"På pappret står det:\n"
		"if(strlen(keyword) + ????? != 30)\n\tdispatch_user();",
		2,
		0,
		{
			{214,0,"Ge ett svar"},
			{32,0,"Chansa"},
		}
	},
	{	// rum 31
		"\"RÄTT SVAR!\"\n"
		"Plötsligt börjar det tjuta. Det tar dock inte långt innan du\n"
		"inser att det bara är väckarklockan som ringer.\n"
		"Med ett suck kan du nu konstatera att mardrömmen är över.\n"
		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Börja om från början"}
		}
	},
	{	// rum 32
		"\"FEL SVAR!\"\n"
		"\"EXEKVERAR NU FUNKTIONEN DISPATCH_USER()\"\n"
		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Börja om från början"}
		}
	},
};


