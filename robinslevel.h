
struct page robin_pages[]=
{
	{
		//Start rum	0
		"Efter att du har klättrat upp för repet kommer du in i ett cirkulärt rum med en stor port framför dig.",	//Rum information
		1,	//antal alternativ
		0,	//nyckel som finns i rummet
		{
			{1,0,"Du går till nästa rum."},
		}
	},
	{
		"Du kommer in i ett rum med en pidestal på mitten av golvet. På pidestalen ligger ett äpple. Du plockar upp äpplet. Du ser att det finns två portar i det här rummet. Den ena fortsätter rakt fram och den andra går till vänster.",	//1
		3,
		4,
		{
			{3,0,"Gå vidare till nästa rum."},
			{0,0,"Gå tillbaka till det cirkulära rummet"},
			{2,0,"Gå in i rummet till vänster"}
		}
	},
	{
		"Det verkar som om att det här rummet håller på att renoveras. Det står en stege lutad mot väggen bredvid en hög med tapetrullar och en hink med tapetklister. Du plockar upp stegen.",	//2
		1,
		5,
		{
			{1,0,"Gå tillbaka till pidestalrummet"}
		}
	},
	{
		"Du kommer in i en jättestor hall. Du kan se att det finns två portar på andra sidan, ungefär 100 meter bort. Det står en vakt framför de två portarna. Han verkar hungrig.",	//3
		3,
		0,
		{
			{5,4,"Du mutar vakten med ett äpple för att kunna ta dörren till vänster."},
			{1,0,"Gå tillbaka till pidestalrummet"},
			{4,4,"Du mutar vakten med ett äpple för att kunna ta dörren till höger"}
		}
	},
	{
		"Du kommer ut på en platform som hänger ovanför en avgrund. Du kan inte se bottnen. När du tar en närmre titt över kanten råkar du stöta till en sten som faller ner. Det tar nästan 30 sekunder innan du hör att den når bottnen. Du börjar hörra trummor. Trummor från djupet.",	//4
		1,
		0,
		{
			{3,0,"Spring tillbaka till den stora hallen"}
		}
	},
	{
		"Du kommer in i vad som ser ut som ett städskåp. Du ser ett hål i taket", //5
		2,
		0,
		{
			{-1,5,"Du ställer stegen mot en hylla och klättrar upp till nästa våning."},
			{3,0,"Gå tillbaka den stora hallen"}
		}
	}
};

/*
   2
   |
 0-1-3-5
	 |
	 4

*/
