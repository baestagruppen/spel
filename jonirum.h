struct page joni_pages[] =
	{	// rum 0
		"Ett felsteg här orsakar mycket irritation och mycket snart står du där utan kroppsdelar\n"
		"Framför dig har du en öppen dörr\n"
		"Bredvid dig har du massa håligheter i marken\n"
		"Om du kollar bakom dig finns det en stor grotta\n"
		"Vad skulle du vilja hitta på?",
		8,
		0,
		{
			{1,0,"Kanske att man skulle vilja gå framåt"}, \\Enda alternativet för överlevnad
			{2,0,"Hoppa ner i grottan bakom dig"}, \\Död
			{3,0,"Gör en bakåtvolt och hoppa ner i en annan liten galengrotta"}, \\Död
			{4,0,"Gör en piruett ner i en stor galengrotta"}, \\Död
			{5,0,"Amputera ditt egna huvud"}, \\Död
			{6,0,"Amputera kroppen"}, \\Död
			{7,0,"Starta om spelet"}, \\Död
			{8,0,"Fuskgenväg"}, \\Död
			
		}
	},
	{	// rum 1
		"Det verkar som om du valde rätt alternativ. Får vi se om du har lika mycket tur fortsättningsvis.\n"
		,
		1,
		0,
		{
			{1,0,"Seså, nu springer vi vidare"}, \\Enda alternativet för överlevnad
			
		}
	},
	
	{	// rum 2
		"En galenräka uppenbarade sig och käkade upp dig, du hade ingen chans \n"

		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Börja om från början"}
		}
	},
	{	// rum 3
		"En galenkrabba delade dig på mitten. \n"

		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Fasiken, då får vi börja om. "}
		}
	},
	{	// rum 4
		"En galenhäst uppenbarade sig och stampade sönder dig. \n"

		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Börja om från början, kom ihåg nu, bli inte sönderstampad igen"}
		}
	},
	{	// rum 5
		"Utan huvud kommer du inte långt, du borde ju inte ens kunna läsa det här. Riktigt galet \n"

		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Bara och starta om, huvudet får du tillbaka."}
		}
	},
	{	// rum 6
		"Utan kropp kommer du inte långt, du borde ju inte ens kunna spela vidare. Riktigt galet \n"

		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Bara och starta om, kroppen får du tillbaka."}
		}
	},
	{	// rum 7
		"Är spelet så kul att du frivilligt startar om? Varsågod! \n"

		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Bara och starta om, tiden får du inte tillbaka."}
		}
	},
	{	// rum 8
		"Trodde du verkligen på att vi på nåt sätt skulle göra det här spelet lättare? \n"

		DEAD_MSG,
		1,
		0,
		{
			{-2,0,"Nu får du börja om, försök inte fuska igen."}
		}
	},
};
// en kommentar
